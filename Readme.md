##Installing Selenium libraries
https://www.selenium.dev/documentation/en/selenium_installation/installing_selenium_libraries/
```
npm install selenium-webdriver
```


##Installing WebDriver binaries
Download the WebDriver binary supported by your browser and place it in the System PATH.
https://www.selenium.dev/documentation/en/webdriver/driver_requirements/

###Run
```
node selenium.js
```

###Unique URL

```
sort -u sum_success >> sum_filter
```

```
awk '{!seen[$0]++};END{for(i in seen) if(seen[i]==1)print i}' sum_success  >> sum_filtered
```

?get Method

Use Regex: \?.*  to replace all vars.
