import { By, until } from 'selenium-webdriver'
export async function countTagsLiveElement(driver, mockData) {
    // Count headings
    const headings = {};
    for (const tag of ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) {
        const tags = await driver.findElements(By.css(tag));
        headings[tag] = tags.length;
    }
    return headings
}
export async function countTagsAEMElement(driver, mockData) {
    // Count headings
    const headings = {};
    for (const tag of ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) {
        const tags = await driver.findElements(By.css(tag));
        headings[tag] = tags.length;
    }
    return headings
}
