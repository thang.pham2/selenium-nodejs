import  { By } from 'selenium-webdriver'
export async function testContentLiveFunc(driver, mockData) {
    const sections = await driver.findElements(By.css('section'));
    let sectionIndex = 1;  // Start section indexing at 1 for readability
    let sectionData = [];
    for (let section of sections) {
        const className = await section.getAttribute('class');
        if (!className.startsWith('MastheadSimple') && !className.startsWith('MastheadCarousel') ) {
            await driver.executeScript(`
            var spans = arguments[0].querySelectorAll('span');
            spans.forEach(span => {
                if (span.textContent.trim() === 'arrow_forward') {
                    span.remove();
                }
            });
        `, section);
            const text = await section.getText();
            if (text.trim().length > 0) {
                sectionData.push({
                    data: text.trim(),
                });
                sectionIndex++;
            }
        }
    }
    return sectionData
}

export async function testContentAEMFunc(driver, mockData) {
    const sections = await driver.findElements(By.css('.cmp-container > .aem-Grid > div.sectioncontainer')); // More specific selector
    let sectionData = [];

    for (let section of sections) {
        const className = await section.getAttribute('class');

        // Ensure it is not a deeper child by checking its immediate parent
        const parent = await section.findElement(By.xpath('..'));
        const parentClassName = await parent.getAttribute('class');

        if (!parentClassName.split(' ').some(cls => cls.startsWith('MastheadSimple') && cls.startsWith('MastheadCarousel'))) {
            await driver.executeScript(`
            var spans = arguments[0].querySelectorAll('span');
            spans.forEach(span => {
                if (span.textContent.trim() === 'arrow_forward') {
                    span.remove();
                }
            });
        `, section);
            const text = await section.getText();
            if (text.trim().length > 0) {
                sectionData.push({
                    data: text.trim(), // Store trimmed text from each section
                });
            }
        }
    }

    return sectionData;
}






