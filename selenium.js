import { Builder, By } from 'selenium-webdriver';
import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
// import testContentFunc from './testcases/content.js';
// import countTagsElement from './testcases/tags.js';
import { mergeDataContent, performAEMTests, performLiveTests, saveResults, setupUrl } from './utils/helper.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const driver = new Builder().forBrowser('firefox').build();
const mixedData = {}

async function checkUrlLiveSite(driver, url, index) {
    const urlPath = url?.loc?.replace(/^.*\/\/[^\/]+/, '');
    await setupUrl(driver, 'https://techcombank.com', urlPath);
    mixedData.resultLive = await performLiveTests(driver);
}

async function checkUrlAemSite(driver, url, index) {
    const urlPath = url?.loc?.replace(/^.*\/\/[^\/]+/, '');
    await setupUrl(driver, 'https://aem-uat.techcombank.com', urlPath);
    mixedData.resultAEM = await performAEMTests(driver);
}

async function processUrls(urls,offset) {
    if(!offset) offset = 0
    // for (let i = 0; i < urls.length; i++) {
    for (let i = offset; i < urls.length; i++) {
        console.log('current index',i)
        // await checkUrl(urls[i], i);
        const mockData=[]

        await checkUrlLiveSite(driver, urls[i], i);
        await checkUrlAemSite(driver, urls[i], i);
        await mergeDataContent(mixedData.resultLive,mixedData.resultAEM,mockData)
        // console.log('mockData',mockData)
        await saveResults(__dirname, i, mockData);
    }
    await driver.quit();
}

const obj = JSON.parse(fs.readFileSync(`${__dirname}/list_url_76.json`, 'utf8'));
console.log('url length', obj.urlset.url.length)
// /khach-hang-ca-nhan/bao-hiem/sinh-trac-tai-chinh/banca-lead
processUrls(obj.urlset.url, 0);
