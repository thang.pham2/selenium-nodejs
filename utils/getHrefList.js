import {By} from "selenium-webdriver";
// .header-navigation
export async function getHrefList(driver, selector) {
    let elements = await driver.findElements(By.css(selector));
    let hrefs = new Set(); // Use a Set to ensure unique href
    for (let element of elements) {
        let href = await element.getAttribute('href');
        if (href && !href.includes('#')) {  // Check if href does not contain '#'
            let cleanedHref = href.replace(/^.*\/\/[^\/]+/, ''); // Remove protocol and domain
            if (cleanedHref.trim() !== '') { // Ensure cleanedHref is not empty
                hrefs.add(cleanedHref);
            }
        }
    }
    // Convert Set to array and sort
    let uniqueHrefs = Array.from(hrefs).sort();
    return uniqueHrefs;
}
export async function fetchScrollHref(driver){

    let elements = await driver.findElements(By.css('a'));
    let hrefs = new Set()
    for (let element of elements) {
        let href = await element.getAttribute('href');
        if (href && href.includes('#')) {  // Check if href starts with '#'
            let cleanedHref = href.replace(/^.*\/\/[^\/]+/, '').split('#')[1];
            if (cleanedHref) {
                hrefs.add(`#${cleanedHref}`); // Add the cleaned href with '#' prefix to the Set
            }
        }
    }
    // Convert Set to array and sort
    let uniqueHrefs = Array.from(hrefs).sort()
    return uniqueHrefs
}
