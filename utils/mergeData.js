import { removeURLs } from "./helper.js"

export async function mergeContentData(resultLive,resultAEM,mockData){
    if(resultLive?.contentData){
        resultLive?.contentData.forEach((el,index)=>{
            mockData.push( {
                TestCase: 'Test_Section_'+(index+1),
                RawJsonRevamp: el.data,
                RawJsonAEM: resultAEM.contentData[index]?.data,
                Status: ''
            })
        })
        }
}
export async function mergeTagsData(resultLive,resultAEM,mockData){
    if(resultLive?.tagsData){
        for (const [key, value] of Object.entries(resultLive?.tagsData)) {
            // console.log(`${key}: ${value}`);
            mockData.push({
                TestCase: 'Test_Tags_'+key,
                RawJsonRevamp: value,
                RawJsonAEM: resultAEM?.tagsData[key],
                Status: ''
            })
          }
        }
}
export async function mergeHrefData(resultLive,resultAEM,mockData){
    if(resultLive?.href){
        // console.log('resultLive?.href',resultLive?.href)
        mockData.push(
        {
            TestCase: 'Test_Href_Count',
            RawJsonRevamp: resultLive.href.count,
            RawJsonAEM: resultAEM.href.count,
            Status: ''
        }
        )
        mockData.push(
        {
            TestCase: 'Test_Href_List',
            RawJsonRevamp: resultLive.href.hrefList,
            RawJsonAEM: resultAEM.href.hrefList,
            Status: ''
        }
        )
    }
}
export async function mergeScrollHrefData(resultLive,resultAEM,mockData){
    if(resultLive?.scrollHref){
        // console.log('resultLive?.scrollHref',resultLive?.scrollHref)
        mockData.push(
            {
                TestCase: 'Test_ScrollHref_Count',
                RawJsonRevamp: resultLive.scrollHref.length,
                RawJsonAEM: resultAEM.scrollHref.length,
                Status: ''
            }
        )
        mockData.push(
            {
                TestCase: 'Test_ScrollHref_List',
                RawJsonRevamp: resultLive.scrollHref.join('\n'),
                RawJsonAEM: resultAEM.scrollHref.join('\n'),
                Status: ''
            }
        )
    }
}


export async function mergeSEOData(resultLive,resultAEM,mockData){
    if(resultLive?.metaObj){
        // console.log('resultLive?.metaObj',resultLive?.metaObj)
        for (const [key, value] of Object.entries(resultLive?.metaObj)) {
            mockData.push({
                TestCase: 'Test_SEO_'+key,
                RawJsonRevamp: removeURLs(value),
                RawJsonAEM: removeURLs(resultAEM?.metaObj[key]),
                Status: ''
            })
        }
        // mockData.push(
        //     {
        //         TestCase: 'Test_ScrollHref_Count',
        //         RawJsonRevamp: resultLive.metaObj.length,
        //         RawJsonAEM: resultAEM.metaObj.length,
        //         Status: ''
        //     }
        // )
    }
}