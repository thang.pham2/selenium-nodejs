import { By, until } from 'selenium-webdriver'
import { countTagsAEMElement, countTagsLiveElement } from '../testcases/tags.js'
import { testContentAEMFunc, testContentLiveFunc } from '../testcases/content.js'
import { mkConfig, generateCsv, asString } from "export-to-csv";
import fs from 'fs';
import {fetchScrollHref, getHrefList} from './getHrefList.js';
import { getObjectSEOtestData } from './getObjectSEO.js';
import { mergeContentData, mergeHrefData, mergeSEOData, mergeScrollHrefData, mergeTagsData } from './mergeData.js';
export async function setupUrl(driver, baseUrl, urlPath) {
    await driver.get(`${baseUrl}${urlPath}`);
    await waitAndFindElement(driver, 'section', 'div.container'); // Wait for page to be fully loaded
    await scrollToBottom(driver); // Scroll to the bottom of the page
    await driver.sleep(1700); // Adjust based on needs
}

export async function performLiveTests(driver, mockData) {
    console.log('performLiveTests')
    const contentData = await testContentLiveFunc(driver);
    const tagsData = await countTagsLiveElement(driver);
    const href= await fetchAndModifyHrefs(driver, 'body .tcb-section  a, body main  a')
    const scrollHref= await fetchScrollHref(driver)

    const metaObj = await getObjectSEOtestData(driver)
    return { 
        contentData, 
        tagsData, 
        href, 
        scrollHref,
        metaObj
     }
}
export async function performAEMTests(driver, mockData) {
    const contentData = await testContentAEMFunc(driver);
    const tagsData = await countTagsAEMElement(driver);
    const href= await fetchAndModifyHrefs(driver, '.root a')
    const scrollHref= await fetchScrollHref(driver)
    
    const metaObj = await getObjectSEOtestData(driver)
    return { 
        contentData, 
        tagsData, 
        href, 
        scrollHref,
        metaObj
    }
}

async function fetchAndModifyHrefs(driver, selector) {
    const hrefList = await getHrefList(driver, selector);

    return {
        count:hrefList.length,
        hrefList:hrefList.join('\n')
    }

}

export async function saveResults(dir, index, mockData) {
    const csvOutput = generateCsv(mkConfig({ useKeysAsHeaders: true }))(mockData);
    const addNewLine = (s) => s + "\n";
    const csvOutputWithNewLine = addNewLine(asString(csvOutput));
    await fs.promises.writeFile(`${dir}/testreport/NO_${index}.csv`, csvOutputWithNewLine);
}

const waitAndFindElement = async (driver, sectionSelector, divSelector) => {
    let sectionElement;

    // Try to locate the section element
    try {
        sectionElement = await driver.wait(
            until.elementLocated(By.css(sectionSelector)),
            10000 // timeout in milliseconds
        );
    } catch (error) {
        console.log("Section element not found, checking for div.section elements instead");
    }

    let divElements;
    if (sectionElement) {
        // If section element is found, find div elements within the section
        divElements = await sectionElement.findElements(By.css(divSelector));
    } else {
        // If section element is not found, find div elements with the given selector
        divElements = await driver.findElements(By.css(divSelector));
    }

    return divElements;
};

async function scrollToBottom(driver) {
    try{
    await driver.executeScript("window.scrollTo(0, document.body.scrollHeight);");
    await driver.sleep(2000); // Adjust the sleep time based on your application's needs
    }catch(err){
        console.log('scrollToBottom error',err)
    }
}


export async function mergeDataContent(resultLive,resultAEM,mockData){
    mergeContentData(resultLive,resultAEM,mockData)
    mergeTagsData(resultLive,resultAEM,mockData)
    mergeHrefData(resultLive,resultAEM,mockData)
    mergeScrollHrefData(resultLive,resultAEM,mockData)
    mergeSEOData(resultLive,resultAEM,mockData)
}

export  function removeURLs(value) {
    return value
      ?.replace(/https:\/\/techcombank.com/g, '')
      ?.replace(/https:\/\/aem-uat.techcombank.com/g, '');
  }