import { By, until } from "selenium-webdriver";

async function getObjectSEO(meta, pageTitle) {
  let metaAmeObj = { title: pageTitle };
  for (let item of meta) {
    try {
      const metaName = await item.getAttribute("name");
      if(metaName=='og:image' || metaName =='next-head-count') continue;
      const metaProperty = await item.getAttribute("property");
      if(metaProperty=='og:image' || metaName =='next-head-count') continue;
      const metaContent = await item.getAttribute("content");
      metaAmeObj = { ...metaAmeObj, [metaName || metaProperty]: metaContent };
    } catch (error) {
      if (error.name === 'StaleElementReferenceError') {
        // Handle stale element reference by re-locating the element
        console.error("Encountered StaleElementReferenceError. Skipping this meta item.");
        continue; // Skipping the stale element
      } else {
        throw error; // Re-throw unexpected errors
      }
    }
  }
  return metaAmeObj;
}

export async function getObjectSEOtestData(driver) {
  const pageTitle = await driver.getTitle();
  const metaData = await driver.findElements(By.css('meta'));
  const result = await getObjectSEO(metaData, pageTitle); // Ensure to await the result
  return result;
}
